# CoRAWeb server
This server implements the datastore component of CoRAWeb. It stores templates, items, and users. 

# Installation

1. Install NodeJs following any tutorial.
2. Install Git (if not installed)
3. Clone the project with `git clone git@bitbucket.org:cora-at-lifia/datastore.git`
4. Install the app with `npm install`
5. Run the server with `node .`

That should be enough to get the server running at `http://localhost:3000`

To test the various REST services, use the explorer at: `http://localhost:3000/explorer`

# Item collection

The datastore is currently configured with templates to extract items from Mendeley, SlideShare and Youtube. 
Normally, extraction templates are used by the item extractor (a plugin running on the browser). 
However, to make tests easier, you can use the bookmarklet. You can find it at `http://localhost:3000`

# Database
The datasources.json file, located in the server directory, indicates where to store items and templates. 
There are two alternatives: 1) in memory (default), 2) MongoDB database. 
Follow the instructions [here](https://loopback.io/doc/en/lb2/datasources.json.html) to configure it. 

# Implementation
The server is built using the [Loopback framework](https://loopback.io).  