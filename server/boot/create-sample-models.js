'use strict';

var samplePeople = [
  {
    "fullName": "Anonymous",
    "email": "no_reply@lifia.info.unlp.edu.ar",
    "groups": ["lifia", "public"]
  }
]

var sampleTemplates = [
  {
    "groups": ["public"],
    "name": "Youtube videos",
    "itemType": "http://schema.org/VideoObject",
    "urlPattern": "https://www.youtube.com/watch.*",
    "owner": "no_reply@lifia.info.unlp.edu.ar",
    "favIcon": "https://s.ytimg.com/yts/img/favicon-vfl8qSV2F.ico",
    "propertySelectors": {
      "title": "//*[@id='container']/h1/yt-formatted-string",
      "description": "//yt-formatted-string[@id='description']"
    }
  },
  {
    "groups": ["public"],
    "name": "Slideshare presentation",
    "itemType": "http://schema.org/PresentationDigitalDocument",
    "urlPattern": "https://www.slideshare.net/.*",
    "owner": "no_reply@lifia.info.unlp.edu.ar",
    "favIcon": "https://public.slidesharecdn.com/favicon.ico",
    "propertySelectors": {
      "title": "//*[@id='main-panel']/div[2]/div[1]/div[1]/div[1]/h1/span",
      "description": "//*[@id='slideshow-description-paragraph']"
    }
  },
  {
    "groups": ["public"],
    "name": "Mendeley article presentation",
    "itemType": "http://schema.org/Article",
    "urlPattern": "https://www.mendeley.com/research-papers/.*",
    "favIcon": "https://www.mendeley.com/themes/custom/mendeleycms/favicon.ico",
    "owner": "no_reply@lifia.info.unlp.edu.ar",
    "propertySelectors": {
      "title": "//*[@id='App']/div/div/div[1]/div/div/div[1]/div[1]/div/div/div/div/h2/span",
      "authors": "//div[@class='authors']",
      "description": "//*[@id='App']/div/div/div[3]/div/div/div[1]/div[1]/p"
    }
  }, {
    "groups": ["public"],
    "name": "Zenodo record",
    "itemType": "http://schema.org/CreativeWork",
    "urlPattern": "https://zenodo.org/record.*",
    "owner": "no_reply@lifia.info.unlp.edu.ar",
    "favIcon": "https://www.mendeley.com/themes/custom/mendeleycms/favicon.ico",
    "propertySelectors": {
      "title": "/html/body/div[2]/div/div[1]/h1",
      "description": "/html/body/div[2]/div/div[1]/p[4]",
    }
  }, {
    "name": "Merlot learning object",
    "urlPattern": "https://www.merlot.org/merlot/viewMaterial.*",
    "propertySelectors": {
      "title": "//div[@class='col detail-title']/h2",
      "description": "//*[@id='material_description']"
    },
    "itemType": "http://schema.org/CreativeWork",
    "groups": [
      "public"
    ],
    "favIcon": "https://www.merlot.org/merlot/images/merlot_column.png",
    "owner": "no_reply@lifia.info.unlp.edu.ar"
  }
];

module.exports = function (app) {
  var ds = app.dataSources.db;
  app.models.Template.findOne(null, function (err, returnedInstance) {
    console.log('Templates exist - skipping boot script');
    if (returnedInstance == null) {
      ds.automigrate(function (err) {
        if (err) throw err;
        app.models.Person.create(samplePeople, function (err, people) {
          if (err) throw err;
          console.log('People created: \n', people);
        });
        app.models.Template.create(sampleTemplates, function (err, templates) {
          if (err) throw err;
          console.log('Templates created: \n', templates);
        });
      });
    }
  });
};
